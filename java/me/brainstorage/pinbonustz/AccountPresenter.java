package me.brainstorage.pinbonustz;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import me.brainstorage.pinbonustz.feeds.FavoritesRssPresenter;

public class AccountPresenter extends ActionBarActivity {
    private Boolean isLogin = false;
    private Boolean showFavoritesList = false;
    private int layout;
    private EditText loginInput;
    private EditText passwordInput;
    private Button loginButton;
    private Button exitButton;
    private FavoritesRssPresenter favoritesRssPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle extras = getIntent().getExtras();

        this.isLogin = extras.getBoolean("isLogin");
        this.showFavoritesList = extras.getBoolean("showFavoritesList");

        this.layout = (this.isLogin)
                ? R.layout.favorites_layout
                : R.layout.login_layout;

        setContentView(this.layout);

        if (this.isLogin && this.showFavoritesList) {
            this.setFavorites();
        } else {
            this.setLogin();
        }
    }

    private void setFavorites() {
        this.favoritesRssPresenter = new FavoritesRssPresenter(
                AccountPresenter.this,
                findViewById(R.id.favoritesNewsList)
        );

        this.exitButton = (Button) findViewById(R.id.exitButton);
        this.exitButton.setOnClickListener(new clickExitListener());
    }

    private void setLogin() {
        this.loginInput = (EditText) findViewById(R.id.loginInput);
        this.passwordInput = (EditText) findViewById(R.id.passwordInput);
        this.loginButton = (Button) findViewById(R.id.loginButton);

        loginButton.setOnClickListener(new clickLoginListener());

        this.loginInput.setOnKeyListener(
                new enterLoginListener(this.loginInput)
        );
        this.passwordInput.setOnKeyListener(
                new enterLoginListener(this.passwordInput)
        );


    }

    private void destroyPresenter() {
        Intent intent = new Intent();
        intent.putExtra("isLogin", this.isLogin);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            destroyPresenter();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem newsAccount = menu.findItem(R.id.accountGoToNews);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.account_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        destroyPresenter();
        return super.onOptionsItemSelected(menuItem);
    }

    public void login(String login, String password) {
        this.isLogin = (login.equals("plic801") && password.equals("123"));
        if (this.isLogin) {
            if (this.showFavoritesList) {
                this.layout = R.layout.favorites_layout;
                setContentView(this.layout);
                this.setFavorites();
            } else {
                destroyPresenter();
            }

        } else {
            // invalid login
            Toast.makeText(getApplicationContext(), R.string.invalid_login_text, Toast.LENGTH_SHORT).show();
        }
    }

    private class clickLoginListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            login(loginInput.getText().toString(),
                    passwordInput.getText().toString()
            );
        }
    }

    private class clickExitListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            destroyPresenter();
        }
    }

    private class enterLoginListener implements EditText.OnKeyListener {
        private EditText input;

        public enterLoginListener(EditText input) {
            this.input = input;
        }


        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if (event != null && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                login(loginInput.getText().toString(),
                        passwordInput.getText().toString()
                );
                InputMethodManager imm = (InputMethodManager) getSystemService(
                        Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
                return true;
            }
            return false;
        }
    }
}
