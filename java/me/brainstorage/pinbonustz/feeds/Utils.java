package me.brainstorage.pinbonustz.feeds;

import android.util.Log;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.sql.Date;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Created by alexandr on 08.04.15.
 */
public class Utils {

    /**
     * <p>Convert String date to Long.<p/>
     * @param {String} date
     * @return {Long}
     */
    public static Long DateToLong(String date) {
        // to GTM
        //"E, dd MMM yyyy HH:mm:ss zzz" (Wed, 26 Jun 2013 06:55:52 GMT)
        Long result = null;
        try {
            DateFormatSymbols dfsFr = new DateFormatSymbols(Locale.ENGLISH);

            SimpleDateFormat sdf = new SimpleDateFormat(
                    "E, dd MMM yyyy HH:mm:ss zzz", dfsFr);

            java.util.Date fromDate = sdf.parse(date);
            java.sql.Date sqlDate = new java.sql.Date(fromDate.getTime());

            result = sqlDate.getTime();
        } catch (ParseException e) {
            Log.e("ERROR", "formatDate", e);
        }
        return result;
    }

    /**
     * <p>Convert long date to string date. (GMT)<p/>
     * @param {Long} date
     * @return {String}
     */
    public static String LongToDate (Long date) {
        return new Date(date).toGMTString();
    }

    /**
     * <p>Return text from tag.</p>
     * @param  {String} tag
     * @params {Element} element
     * @return {String}
     */
    public static String getTextContent(String tag, Element element) {
        NodeList tagNode = element.getElementsByTagName(tag);
        Element tagElement = (Element) ((tagNode.getLength() > 0)
                ? tagNode.item(0)
                : null);

        return (tagElement != null && tagElement.getChildNodes().getLength() > 0)
                ? tagElement.getTextContent()
                : null;
    }
}
