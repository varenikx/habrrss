package me.brainstorage.pinbonustz.feeds;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import org.w3c.dom.Element;

import org.w3c.dom.NodeList;


/**
 * <p>Rss channel.<p/>
 *
 * @param {Element} element
 * @autor Created by alexandr on 05.04.15.
 * @class me.brainstorage.pinbonustz.feeds.RssChannel
 */
public class RssChannelModel {
    private String link, title, description, language, pubDate, lastBuildDate,
            docs, generator, managingEditor, feed;
    private Element element;
    private ArrayList<RssItemModel> items;
    private Integer id;
    /* Public */

    public RssChannelModel fillFromDom(Element element) {

        this.setElement(element);
        this.setLink(Utils.getTextContent("link", element));
        this.setTitle(Utils.getTextContent("title", element));
        this.setDescription(Utils.getTextContent("description", element));
        this.setLanguage(Utils.getTextContent("language", element));
        this.setPubDate(Utils.getTextContent("pubDate", element));
        this.setLastBuildDate(Utils.getTextContent("lastBuildDate", element));
        this.setDocs(Utils.getTextContent("docs", element));
        this.setGenerator(Utils.getTextContent("generator", element));
        this.setManagingEditor(Utils.getTextContent("managingEditor", element));

        this.setItems(this.getChannelItems());

        return this;
    }

    /* Private */

    /**
     * <p>Return channel items.</p>
     *
     * @return {ArrayList<RssItem>}
     */
    private ArrayList<RssItemModel> getChannelItems() {
        NodeList items = this.element.getElementsByTagName("item");
        ArrayList<RssItemModel> result = new ArrayList<RssItemModel>();

        for (int i = 0; i < items.getLength(); i++) {
            RssItemModel item = new RssItemModel();
            item.fillFromDom((Element) items.item(i));

            result.add(item);
        }

        return result;
    }

    /* Public */

    public Element getElement() {
        return this.element;
    }

    public ArrayList<RssItemModel> getItems() {
        return this.items;
    }

    public String getPubDate() {
        return this.pubDate;
    }

    public String getLink() {
        return this.link;
    }

    public String getDescription() {
        return this.description;
    }

    public String getDocs() {
        return this.docs;
    }

    public String getGenerator() {
        return this.generator;
    }

    public String getLanguage() {
        return this.language;
    }

    public String getLastBuildDate() {
        return this.lastBuildDate;
    }

    public String getManagingEditor() {
        return this.managingEditor;
    }

    public String getTitle() {
        return this.title;
    }

    public String getFeed() {
        return this.feed;
    }

    public Integer getId() {
        return this.id;
    }

    public void setElement(Element element) {
        this.element = element;
    }

    public void setItems(ArrayList<RssItemModel> items) {
        // sort items
        Collections.sort(items, new Comparator<RssItemModel>() {
            @Override
            public int compare(RssItemModel lhs, RssItemModel rhs) {
                Integer r = (int) (long) Utils.DateToLong(rhs.getPubDate());
                Integer l = (int) (long) Utils.DateToLong(lhs.getPubDate());
                return r.compareTo(l);
            }
        });

        this.items = items;
    }

    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDocs(String docs) {
        this.docs = docs;
    }

    public void setGenerator(String generator) {
        this.generator = generator;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public void setLastBuildDate(String lastBuildDate) {
        this.lastBuildDate = lastBuildDate;
    }

    public void setManagingEditor(String managingEditor) {
        this.managingEditor = managingEditor;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setFeed(String feed) {
        this.feed = feed;
    }
}

