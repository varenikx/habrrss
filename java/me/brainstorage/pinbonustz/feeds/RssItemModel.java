package me.brainstorage.pinbonustz.feeds;

import org.w3c.dom.Element;

/**
 * <p>Rss chanel item.</p>
 *
 * @param {Element} element item node
 * @autor Created by alexandr on 05.04.15.
 * @class me.brainstorage.pinbonustz.feeds.RssItem
 */
public class RssItemModel {
    private String title, link, description, pubDate, guid, author;
    private Element element;
    private Integer channelId;
    private Integer id;

    /* Public */

    public RssItemModel fillFromDom(Element element) {

        this.setElement(element);

        this.setTitle(Utils.getTextContent("title", element));
        this.setLink(Utils.getTextContent("link", element));
        this.setDescription(Utils.getTextContent("description", element));
        this.setPubDate(Utils.getTextContent("pubDate", element));
        this.setGuid(Utils.getTextContent("guid", element));
        this.setAuthor(Utils.getTextContent("author", element));

        return this;
    }

    public Element getElement() {
        return this.element;
    }

    public String getTitle() {
        return this.title;
    }

    public String getAuthor() {
        return this.author;
    }

    public String getDescription() {
        return this.description;
    }

    public String getGuid() {
        return this.guid;
    }

    public String getLink() {
        return this.link;
    }

    public String getPubDate() {
        return this.pubDate;
    }

    public Integer getChannelId() {
        return this.channelId;
    }

    public Integer getId() {
        return this.id;
    }

    public RssItemModel setElement(Element element) {
        this.element = element;
        return this;
    }

    public RssItemModel setTitle(String title) {
        this.title = title;
        return this;
    }

    public RssItemModel setAuthor(String author) {
        this.author = author;
        return this;
    }

    public RssItemModel setDescription(String description) {
        this.description = description;
        return this;
    }

    public RssItemModel setGuid(String guid) {
        this.guid = guid;
        return this;
    }

    public RssItemModel setLink(String link) {
        this.link = link;
        return this;
    }

    public RssItemModel setPubDate(String pubDate) {
        this.pubDate = pubDate;
        return this;
    }

    public RssItemModel setChannelId(Integer channelId) {
        this.channelId = channelId;
        return this;
    }

    public RssItemModel setId(Integer itemId) {
        this.id = itemId;
        return this;
    }
}
