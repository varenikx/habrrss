package me.brainstorage.pinbonustz.feeds;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by alexandr on 06.04.15.
 */

public class LocalRssPresenter implements RssPresenter.LocalFeedsInterfaces {
    private Context context;
    private DBHelper mDBHelper = null;
    private SQLiteDatabase db = null;

    public static final String CHANNELS_TABLE = "CHANNELS";
    public static final String CHANNELS_FEED_COLUMN = "FEED";
    public static final String CHANNELS_LINK_COLUMN = "LINK";
    public static final String CHANNELS_TITLE_COLUMN = "TITLE";
    public static final String CHANNELS_DESCRIPTION_COLUMN = "DESC";
    public static final String CHANNELS_LANGUAGE_COLUMN = "LANG";
    public static final String CHANNELS_PUBDATE_COLUMN = "PDATE";
    public static final String CHANNELS_LASTBUILDDATE_COLUMN = "LBDATE";
    public static final String CHANNELS_DOCS_COLUMN = "DOCS";
    public static final String CHANNELS_GENERATOR_COLUMN = "GEN";
    public static final String CHANNELS_MANAGINGEDITOR_COLUMN = "EDITOR";
    public static final String CHANNELS_WEBMASTER_COLUMN = "WEBMASTER";

    public static final String ITEMS_TABLE = "ITEMS";
    public static final String ITEMS_TITLE_COLUMN = "TITLE";
    public static final String ITEMS_LINK_COLUMN = "LINK";
    public static final String ITEMS_DESCRIPTION_COLUMN = "DESC";
    public static final String ITEMS_PUBDATE_COLUMN = "PDATE";
    public static final String ITEMS_GUID_COLUMN = "GUID";
    public static final String ITEMS_AUTOR_COLUMN = "AUTOR";
    public static final String ITEMS_CHANNEL_COLUMN = "CHANNEL";

    public static final String FAVORITES_TABLE = "FAVORITES";
    public static final String FAVORITES_TITLE_COLUMN = "TITLE";
    public static final String FAVORITES_LINK_COLUMN = "LINK";
    public static final String FAVORITES_DESCRIPTION_COLUMN = "DESC";
    public static final String FAVORITES_PUBDATE_COLUMN = "PDATE";
    public static final String FAVORITES_GUID_COLUMN = "GUID";
    public static final String FAVORITES_AUTOR_COLUMN = "AUTOR";
    public static final String FAVORITES_CHANNEL_COLUMN = "CHANNEL";

    public LocalRssPresenter(Context context) {
        this.context = context;
        this.mDBHelper = new DBHelper(this.context);
        this.db = this.mDBHelper.getWritableDatabase();
    }

    public Integer addChannelItem(Integer channelId, RssItemModel item) {
        ContentValues itemsContent = new ContentValues();

        itemsContent.put(ITEMS_CHANNEL_COLUMN, channelId);
        itemsContent.put(ITEMS_DESCRIPTION_COLUMN, item.getDescription());
        itemsContent.put(ITEMS_GUID_COLUMN, item.getGuid());
        itemsContent.put(ITEMS_LINK_COLUMN, item.getLink());
        itemsContent.put(ITEMS_PUBDATE_COLUMN, item.getPubDate());
        itemsContent.put(ITEMS_AUTOR_COLUMN, item.getAuthor());
        itemsContent.put(ITEMS_TITLE_COLUMN, item.getTitle());

        return (int) this.getDB().insert(ITEMS_TABLE, null, itemsContent);
    }

    public Integer addFavoritesItem(RssItemModel item) {
        ContentValues itemsContent = new ContentValues();

        itemsContent.put(ITEMS_CHANNEL_COLUMN, 0);
        itemsContent.put(FAVORITES_DESCRIPTION_COLUMN, item.getDescription());
        itemsContent.put(FAVORITES_GUID_COLUMN, item.getGuid());
        itemsContent.put(FAVORITES_LINK_COLUMN, item.getLink());
        itemsContent.put(FAVORITES_PUBDATE_COLUMN, item.getPubDate());
        itemsContent.put(FAVORITES_AUTOR_COLUMN, item.getAuthor());
        itemsContent.put(FAVORITES_TITLE_COLUMN, item.getTitle());

        return (int) this.getDB().insert(FAVORITES_TABLE, null, itemsContent);
    }

    public void addChannelItems(Integer channelId, ArrayList<RssItemModel> items) {
        for (int i = 0; i < items.size(); i++) {
            this.addChannelItem(channelId, items.get(i));
        }
    }

    public void updateChannelItems(Integer channelId, ArrayList<RssItemModel> items) {

        this.getDB().delete(
                ITEMS_TABLE,
                "_id = ? ",
                new String[]{String.valueOf(channelId)});

        this.addChannelItems(channelId, items);
    }

    public Integer addChannel(RssChannelModel channel) {
        StringBuilder sqlBuilder = new StringBuilder();
        ContentValues channelContent = new ContentValues();

        channelContent.put(CHANNELS_LINK_COLUMN, channel.getLink());
        channelContent.put(CHANNELS_DESCRIPTION_COLUMN, channel.getDescription());
        channelContent.put(CHANNELS_DOCS_COLUMN, channel.getDocs());
        channelContent.put(CHANNELS_GENERATOR_COLUMN, channel.getGenerator());
        channelContent.put(CHANNELS_LANGUAGE_COLUMN, channel.getLanguage());
        channelContent.put(CHANNELS_LASTBUILDDATE_COLUMN, channel.getLastBuildDate());
        channelContent.put(CHANNELS_MANAGINGEDITOR_COLUMN, channel.getManagingEditor());
        channelContent.put(CHANNELS_PUBDATE_COLUMN, channel.getPubDate());
        channelContent.put(CHANNELS_TITLE_COLUMN, channel.getTitle());
        channelContent.put(CHANNELS_FEED_COLUMN, channel.getFeed());

        return (int) this.getDB().insert(CHANNELS_TABLE, null, channelContent);
    }

    public ArrayList<Integer> updateChannels(ArrayList<RssChannelModel> channels) {
        ArrayList<Integer> result = new ArrayList<Integer>();

        if (channels.size() > 0) {
            this.getDB().delete(CHANNELS_TABLE, null, null);
        }

        for (int i = 0; i < channels.size(); i++) {
            Integer channelId = this.addChannel(channels.get(i));

            if (channelId > 0) {
                ArrayList<RssItemModel> channelItems = channels.get(i).getItems();
                if (channelItems.size() > 0) {
                    this.addChannelItems(channelId, channelItems);
                }
            }
            result.add(channelId);
        }
        return result;
    }

    public ArrayList<RssChannelModel> getChannels(String feedUrl) {

        Cursor channelCursor = this.getDB().query(
                CHANNELS_TABLE,
                null,
                CHANNELS_FEED_COLUMN + " =  ? ",
                new String[]{feedUrl}, null, null, null);

        ArrayList<RssChannelModel> channels = new ArrayList<RssChannelModel>();

        while (channelCursor.moveToNext()) {
            RssChannelModel channel = new RssChannelModel();

            channel.setId(
                    channelCursor.getInt(
                            channelCursor.getColumnIndex("_id")
                    )
            );
            channel.setFeed(
                    channelCursor.getString(
                            channelCursor.getColumnIndex(CHANNELS_FEED_COLUMN)
                    )
            );
            channel.setLink(
                    channelCursor.getString(
                            channelCursor.getColumnIndex(CHANNELS_LINK_COLUMN)
                    )
            );

            channel.setDescription(
                    channelCursor.getString(
                            channelCursor.getColumnIndex(CHANNELS_DESCRIPTION_COLUMN)
                    )
            );
            channel.setTitle(
                    channelCursor.getString(
                            channelCursor.getColumnIndex(CHANNELS_TITLE_COLUMN)
                    )
            );
            channel.setLanguage(
                    channelCursor.getString(
                            channelCursor.getColumnIndex(CHANNELS_LANGUAGE_COLUMN)
                    )
            );
            channel.setPubDate(
                    channelCursor.getString(
                            channelCursor.getColumnIndex(CHANNELS_PUBDATE_COLUMN)
                    )
            );
            channel.setLastBuildDate(
                    channelCursor.getString(
                            channelCursor.getColumnIndex(CHANNELS_LASTBUILDDATE_COLUMN)
                    )
            );
            channel.setDocs(
                    channelCursor.getString(
                            channelCursor.getColumnIndex(CHANNELS_DOCS_COLUMN)
                    )
            );
            channel.setGenerator(
                    channelCursor.getString(
                            channelCursor.getColumnIndex(CHANNELS_GENERATOR_COLUMN)
                    )
            );
            channel.setManagingEditor(
                    channelCursor.getString(
                            channelCursor.getColumnIndex(CHANNELS_MANAGINGEDITOR_COLUMN)
                    )
            );
            channel.setItems(this.getChannelItems(
                    channel.getId()
            ));

            channels.add(channel);
        }

        channelCursor.close();
        return channels;
    }

    public ArrayList<RssItemModel> getChannelItems(Integer chanelId) {
        Cursor itemsCursor = this.getDB().query(
                ITEMS_TABLE,
                null,
                ITEMS_CHANNEL_COLUMN + " =  ? ",
                new String[]{Integer.toString(chanelId)}, null, null, null);

        ArrayList<RssItemModel> items = new ArrayList<RssItemModel>();

        while (itemsCursor.moveToNext()) {
            RssItemModel item = new RssItemModel();

            item.setTitle(
                    itemsCursor.getString(
                            itemsCursor.getColumnIndex(ITEMS_TITLE_COLUMN)
                    )
            );
            item.setLink(
                    itemsCursor.getString(
                            itemsCursor.getColumnIndex(ITEMS_LINK_COLUMN)
                    )
            );
            item.setDescription(
                    itemsCursor.getString(
                            itemsCursor.getColumnIndex(ITEMS_DESCRIPTION_COLUMN)
                    )
            );
            item.setPubDate(
                    itemsCursor.getString(
                            itemsCursor.getColumnIndex(ITEMS_PUBDATE_COLUMN)
                    )
            );
            item.setGuid(
                    itemsCursor.getString(
                            itemsCursor.getColumnIndex(ITEMS_GUID_COLUMN)
                    )
            );
            item.setAuthor(
                    itemsCursor.getString(
                            itemsCursor.getColumnIndex(ITEMS_AUTOR_COLUMN)
                    )
            );
            item.setChannelId(
                    itemsCursor.getInt(
                            itemsCursor.getColumnIndex(ITEMS_CHANNEL_COLUMN)
                    )
            );
            items.add(item);
        }
        itemsCursor.close();
        return items;
    }

    public Integer removeFavoritesItem(String url) {

        return this.getDB().delete(
                FAVORITES_TABLE,
                FAVORITES_LINK_COLUMN + " = ? ",
                new String[]{url}
        );

    }

    public ArrayList<RssItemModel> getFavoritesItems() {
        Cursor itemsCursor = this.getDB().query(
                FAVORITES_TABLE,
                null,
                null,
                null,
                null,
                null,
                null
        );

        ArrayList<RssItemModel> items = new ArrayList<RssItemModel>();

        while (itemsCursor.moveToNext()) {
            RssItemModel item = new RssItemModel();

            item.setId(
                    itemsCursor.getInt(
                            itemsCursor.getColumnIndex("_id")
                    )
            );
            item.setTitle(
                    itemsCursor.getString(
                            itemsCursor.getColumnIndex(FAVORITES_TITLE_COLUMN)
                    )
            );
            item.setLink(
                    itemsCursor.getString(
                            itemsCursor.getColumnIndex(FAVORITES_LINK_COLUMN)
                    )
            );
            item.setDescription(
                    itemsCursor.getString(
                            itemsCursor.getColumnIndex(FAVORITES_DESCRIPTION_COLUMN)
                    )
            );
            item.setPubDate(
                    itemsCursor.getString(
                            itemsCursor.getColumnIndex(FAVORITES_PUBDATE_COLUMN)
                    )
            );
            item.setGuid(
                    itemsCursor.getString(
                            itemsCursor.getColumnIndex(FAVORITES_GUID_COLUMN)
                    )
            );
            item.setAuthor(
                    itemsCursor.getString(
                            itemsCursor.getColumnIndex(FAVORITES_AUTOR_COLUMN)
                    )
            );
            item.setChannelId(
                    itemsCursor.getInt(
                            itemsCursor.getColumnIndex(FAVORITES_CHANNEL_COLUMN)
                    )
            );
            items.add(item);
        }
        itemsCursor.close();
        return items;
    }

    public Boolean isFavorite(String url) {
        Boolean result = false;
        Cursor itemsCursor = this.getDB().query(
                FAVORITES_TABLE,
                new String[]{"_id"},
                FAVORITES_LINK_COLUMN + " = ? ",
                new String[]{url},
                null,
                null,
                null
        );
        if (itemsCursor.moveToNext()) {
            result = true;
        }
        return result;
    }

    public SQLiteDatabase getDB() {
        if (this.db == null) {
            this.mDBHelper = new DBHelper(this.context);
            this.db = this.mDBHelper.getWritableDatabase();
        }
        return this.db;
    }

    public void closeDB() {
        if (this.mDBHelper != null) {
            try {
                this.mDBHelper.close();
            } catch (Exception e) {
                Log.e("ERROR", "closeDB", e);
            }
        }
    }

    private class DBHelper extends SQLiteOpenHelper {
        private final static int DB_VERSION = 1;
        private final static String DB_NAME = "tz.db";

        Context context;

        public DBHelper(Context context) {
            super(context, DB_NAME, null, DB_VERSION);
            this.context = context;
        }

        @Override
        public void onCreate(SQLiteDatabase db) {

            StringBuilder sqlBuilder = new StringBuilder();

            // CHANNELS TABLE
            sqlBuilder.append("CREATE TABLE ").append(CHANNELS_TABLE).append(" (")
                    .append("_id INTEGER PRIMARY KEY AUTOINCREMENT, ")
                    .append(CHANNELS_FEED_COLUMN).append(" TEXT, ")
                    .append(CHANNELS_LINK_COLUMN).append(" TEXT, ")
                    .append(CHANNELS_DESCRIPTION_COLUMN).append(" TEXT, ")
                    .append(CHANNELS_DOCS_COLUMN).append(" TEXT, ")
                    .append(CHANNELS_GENERATOR_COLUMN).append(" TEXT, ")
                    .append(CHANNELS_LANGUAGE_COLUMN).append(" TEXT, ")
                    .append(CHANNELS_LASTBUILDDATE_COLUMN).append(" TEXT, ")
                    .append(CHANNELS_MANAGINGEDITOR_COLUMN).append(" TEXT, ")
                    .append(CHANNELS_PUBDATE_COLUMN).append(" TEXT, ")
                    .append(CHANNELS_TITLE_COLUMN).append(" TEXT, ")
                    .append(CHANNELS_WEBMASTER_COLUMN).append(" TEXT);");

            db.execSQL(sqlBuilder.toString());

            //ITEMS TABLE
            sqlBuilder.setLength(0);
            sqlBuilder.append("CREATE TABLE ").append(ITEMS_TABLE).append(" (")
                    .append("_id INTEGER PRIMARY KEY AUTOINCREMENT, ")
                    .append(ITEMS_CHANNEL_COLUMN).append(" INTEGER, ")
                    .append(ITEMS_DESCRIPTION_COLUMN).append(" TEXT, ")
                    .append(ITEMS_GUID_COLUMN).append(" TEXT, ")
                    .append(ITEMS_LINK_COLUMN).append(" TEXT, ")
                    .append(ITEMS_PUBDATE_COLUMN).append(" TEXT, ")
                    .append(ITEMS_AUTOR_COLUMN).append(" TEXT, ")
                    .append(ITEMS_TITLE_COLUMN).append(" TEXT);");

            db.execSQL(sqlBuilder.toString());
            //FAVORITES TABLE
            sqlBuilder.setLength(0);
            sqlBuilder.append("CREATE TABLE ").append(FAVORITES_TABLE).append(" (")
                    .append("_id INTEGER PRIMARY KEY AUTOINCREMENT, ")
                    .append(FAVORITES_CHANNEL_COLUMN).append(" INTEGER, ")
                    .append(FAVORITES_DESCRIPTION_COLUMN).append(" TEXT, ")
                    .append(FAVORITES_GUID_COLUMN).append(" TEXT, ")
                    .append(FAVORITES_LINK_COLUMN).append(" TEXT, ")
                    .append(FAVORITES_PUBDATE_COLUMN).append(" TEXT, ")
                    .append(FAVORITES_AUTOR_COLUMN).append(" TEXT, ")
                    .append(FAVORITES_TITLE_COLUMN).append(" TEXT);");

            db.execSQL(sqlBuilder.toString());
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // update algorithm
        }
    }
}
