package me.brainstorage.pinbonustz.feeds;

import android.content.Context;
import android.view.View;
import me.brainstorage.pinbonustz.views.NewsListView;

import java.util.ArrayList;

/**
 * <p>Rss presenter.</p>
 *
 * @param {DB} database
 * @author Created by alexandr on 05.04.15.
 * @class me.brainstorage.pinbonustz.feeds.FavoritesRssPresenter
 */
public class FavoritesRssPresenter {

    private Context context;

    private LocalRssPresenter localRss;
    private View rssView;


    public FavoritesRssPresenter(Context context, View rssView) {
        this.context = context;

        this.localRss = new LocalRssPresenter(context);
        this.rssView = rssView;

        this.getFavoritesRss();
    }

    public void getFavoritesRss() {
        ArrayList<RssItemModel> items = this.localRss.getFavoritesItems();
        if (items.size() > 0) {
            this.setRssItemsToView(items);
        }
    }

    private void setRssItemsToView(ArrayList<RssItemModel> news) {
        NewsListView favoritesView = (NewsListView) this.rssView;
        favoritesView.setNews(news);
    }
}
