package me.brainstorage.pinbonustz.feeds;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import me.brainstorage.pinbonustz.views.NewsChannelsView;
import org.w3c.dom.Document;

import java.util.ArrayList;

/**
 * <p>Rss presenter.</p>
 *
 * @param {DB} database
 * @author Created by alexandr on 05.04.15.
 * @class me.brainstorage.pinbonustz.feeds.RssPresenter
 */
public class RssPresenter {

    private Context context;
    private String feedUrl;
    private RemoteRssPresenter remoteRss;
    private LocalRssPresenter localRss;
    private View rssView;
    private AdapterView.OnItemClickListener newsTap;
    private AdapterView.OnItemLongClickListener newsLongTap;

    public RssPresenter(Context context, View rssView, String feedUrl, AdapterView.OnItemClickListener newsTapListener,
                        AdapterView.OnItemLongClickListener newsLongTapListener) {
        this.context = context;
        this.feedUrl = feedUrl;
        this.remoteRss = new RemoteRssPresenter(this);
        this.localRss = new LocalRssPresenter(context);
        this.rssView = rssView;
        this.newsTap = newsTapListener;
        this.newsLongTap = newsLongTapListener;
    }

    public void setActualRss() {
        this.getLocalRss();
        this.refreshRss();
    }

    public void refreshRss() {
        this.remoteRss.getRemoteRss(this.feedUrl);
    }

    protected void onRemoteRssCompleted(ArrayList<RssChannelModel> channels) {
        // set to this.rssView and save local
        if (channels.size() > 0) {
            this.localRss.updateChannels(channels);
            this.setRssChannelsToView(channels);
        }
    }

    public void getLocalRss() {
        // get local channels (on rss url) and set to this.rssView
        ArrayList<RssChannelModel> channels = this.localRss.getChannels(this.feedUrl);
        if (channels.size() > 0) {
            this.setRssChannelsToView(channels);
        }
    }

    private void setRssChannelsToView(ArrayList<RssChannelModel> channels) {
        NewsChannelsView channelsView = (NewsChannelsView) this.rssView;
        channelsView.setChannels(channels, this.newsTap, this.newsLongTap);
    }

    public static interface LocalFeedsInterfaces {

        public Integer addChannelItem(Integer channelId, RssItemModel item);

        public void addChannelItems(Integer channelId, ArrayList<RssItemModel> items);

        public void updateChannelItems(Integer channelId, ArrayList<RssItemModel> items);

        public Integer addChannel(RssChannelModel channel);

        public ArrayList<Integer> updateChannels(ArrayList<RssChannelModel> channels);

        public ArrayList<RssChannelModel> getChannels(String feedUrl);

        public ArrayList<RssItemModel> getChannelItems(Integer chanelId);

    }

    public static interface RemoteFeedsInterfaces {

        public void getRemoteRss(String url);

        public void onRemoteRssCompleted(ArrayList<RssChannelModel> channels);

        public void onXmlReceived(Document document);

    }

}
