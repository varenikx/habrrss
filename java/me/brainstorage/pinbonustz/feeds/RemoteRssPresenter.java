package me.brainstorage.pinbonustz.feeds;

import android.os.AsyncTask;
import android.util.Log;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;

/**
 * Created by alexandr on 07.04.15.
 */
public class RemoteRssPresenter implements RssPresenter.RemoteFeedsInterfaces {

    RssPresenter rssPresenter;
    String feedUrl;

    public RemoteRssPresenter(RssPresenter rssPresenter) {
        this.rssPresenter = rssPresenter;
    }

    public void getRemoteRss(String url) {
        this.feedUrl = url;
        new AsyncGetRemoteRss(url, this).execute();
    }

    public void onXmlReceived(Document document) {
        new AsyncParseXmlDocument(document, this.feedUrl, this).execute();
    }

    public void onRemoteRssCompleted(ArrayList<RssChannelModel> channels) {
        this.rssPresenter.onRemoteRssCompleted(channels);
    }

    /**
     * <p>XML parser.</p>
     *
     * @class me.brainstorage.pinbonustz.feeds.RssPresenter.ParseXmlDocument
     * @extends AsyncTask
     */
    private class AsyncParseXmlDocument extends AsyncTask<Void, Void, ArrayList<RssChannelModel>> {
        private RemoteRssPresenter presenter;
        private Document xmlDocument;
        private String feedUrl;

        public AsyncParseXmlDocument(Document xmlDocument, String feedUrl, RemoteRssPresenter presenter) {
            this.feedUrl = feedUrl;
            this.presenter = presenter;
            this.xmlDocument = xmlDocument;
        }

        @Override
        protected ArrayList<RssChannelModel> doInBackground(Void... params) {
            return this.getChannels(this.xmlDocument);
        }

        @Override
        protected void onPostExecute(ArrayList<RssChannelModel> result) {
            this.presenter.onRemoteRssCompleted(result);
        }

        /**
         * <p>Get all channels in XML.</p>
         *
         * @param {Document} document
         * @return {ArrayList<RssChannel>}
         */
        private ArrayList<RssChannelModel> getChannels(Document document) {
            ArrayList result = new ArrayList();
            NodeList channels = document.getElementsByTagName("channel");

            for (int i = 0; i < channels.getLength(); i++) {
                RssChannelModel channel = new RssChannelModel();
                channel.fillFromDom((Element) channels.item(i));
                channel.setFeed(this.feedUrl);

                result.add(channel);
            }
            return result;
        }
    }

    /**
     * <p>Load remoure xml file.</p>
     *
     * @class me.brainstorage.pinbonustz.feeds.RssPresenter.GetRemouteRss
     * @extends AsyncTask
     */
    private class AsyncGetRemoteRss extends AsyncTask<Void, Void, Document> {
        private String url;
        private RemoteRssPresenter presenter;

        public AsyncGetRemoteRss(String url, RemoteRssPresenter presenter) {
            this.url = url;
            this.presenter = presenter;
        }

        @Override
        protected Document doInBackground(Void... params) {
            String response = null;

            try {
                response = EntityUtils.toString(
                        new DefaultHttpClient().execute(
                                new HttpGet(this.url)).getEntity(),
                        "UTF-8"
                );

                if (response.length() > 0) {
                    try {
                        DocumentBuilder xmlDocumentBuilder = DocumentBuilderFactory
                                .newInstance()
                                .newDocumentBuilder();

                        InputSource inputXmlSource = new InputSource(
                                new StringReader(response)
                        );

                        try {
                            Document xmlDocument = xmlDocumentBuilder.parse(inputXmlSource);
                            xmlDocument.getDocumentElement().normalize();
                            return xmlDocument;
                        } catch (SAXException e) {
                            Log.e("ERROR", "SAXException", e);
                        }
                    } catch (ParserConfigurationException e) {
                        Log.e("ERROR", "ParserConfigurationException", e);
                    }
                }
            } catch (ClientProtocolException e) {
                Log.e("ERROR", "ClientProtocolException", e);
            } catch (IOException e) {
                Log.e("ERROR", "IOException", e);
            }
            return null;
        }


        @Override
        protected void onPostExecute(Document result) {
            // handler result
            this.presenter.onXmlReceived(result);
        }

    }

}
