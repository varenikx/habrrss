package me.brainstorage.pinbonustz;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.Toast;
import me.brainstorage.pinbonustz.feeds.LocalRssPresenter;
import me.brainstorage.pinbonustz.feeds.RssItemModel;
import me.brainstorage.pinbonustz.feeds.RssPresenter;


public class NewsPresenter extends ActionBarActivity implements View.OnClickListener {
    private RssPresenter rssPresenter;
    private Intent detailsPresenter = null;
    private LocalRssPresenter localRssPresenter;
    private Intent accountPresenter;
    private Boolean isLogin = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.channel_list_layout);

        this.localRssPresenter = new LocalRssPresenter(
                NewsPresenter.this
        );

        this.rssPresenter = new RssPresenter(
                getBaseContext(),
                findViewById(R.id.channelsList),
                "http://habrahabr.ru/rss/hubs/",
                new newsTap(),
                new newsLongTap()
        );

        this.rssPresenter.getLocalRss();

        Button refreshControl = (Button) findViewById(R.id.refresh);
        refreshControl.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.refresh:
                rssPresenter.refreshRss();
                break;
            default:
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (this.isNetworkAvailable()) {
            this.rssPresenter.refreshRss();
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (!this.isLogin) {
            MenuItem newsAccount = menu.findItem(R.id.newsAccount);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.news_menu_layout, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        getAccount(true);
        return super.onOptionsItemSelected(menuItem);
    }

    private void getAccount(Boolean showFavoritesList) {
        Intent intent = this.getAccountPresenter();
        intent.putExtra("isLogin", this.isLogin);
        intent.putExtra("showFavoritesList", showFavoritesList);

        startActivityForResult(intent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {
            return;
        }
        this.isLogin = data.getBooleanExtra("isLogin", false);
    }

    private void openNewsDetails(RssItemModel item) {
        Intent intent = this.getDetailsPresenter();

        intent.putExtra("desc", item.getDescription());
        intent.putExtra("guid", item.getGuid());
        intent.putExtra("link", item.getLink());
        intent.putExtra("pdate", item.getPubDate());
        intent.putExtra("autor", item.getAuthor());
        intent.putExtra("title", item.getTitle());
        intent.putExtra("isLogin", this.isLogin);

        startActivityForResult(intent, 1);
    }

    private Intent getDetailsPresenter() {
        if (this.detailsPresenter == null) {
            this.detailsPresenter = new Intent(getBaseContext(), DetailsPresenter.class);
            this.detailsPresenter.addFlags(
                    Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP
            );
        }
        return this.detailsPresenter;
    }

    private Intent getAccountPresenter() {
        if (this.accountPresenter == null) {
            this.accountPresenter = new Intent(getBaseContext(), AccountPresenter.class);
            this.accountPresenter.addFlags(
                    Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP
            );
        }
        return this.accountPresenter;
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager networkManager = (ConnectivityManager) getSystemService(
                Context.CONNECTIVITY_SERVICE
        );
        Boolean result = false;
        if (networkManager != null) {
            NetworkInfo[] netInfo = networkManager.getAllNetworkInfo();
            if (netInfo != null) {
                for (NetworkInfo ni : netInfo) {
                    if (ni.isConnected()) {
                        result = true;
                    }
                }

            }
        }
        return result;
    }

    private class newsTap implements AdapterView.OnItemClickListener {
        public void onItemClick(AdapterView<?> parent, View view,
                                int pos, long id) {
            RssItemModel item = (RssItemModel) parent.getItemAtPosition(pos);
            openNewsDetails(item);
        }
    }

    private class newsLongTap implements AdapterView.OnItemLongClickListener {
        public boolean onItemLongClick(AdapterView<?> parent, View arg1,
                                       int pos, long id) {
            RssItemModel item = (RssItemModel) parent.getItemAtPosition(pos);
            showMenu(item, arg1);
            return true;
        }
    }

    private void showMenu(final RssItemModel item, View anchor) {

        PopupMenu popupMenu = new PopupMenu(this, anchor);
        popupMenu.inflate(R.menu.news_popup_menu_layout);

        MenuItem addFavorites = popupMenu.getMenu().findItem(R.id.addFavorites);
        MenuItem removeFavorites = popupMenu.getMenu().findItem(R.id.removeFavorites);

        Boolean isFavorite = localRssPresenter.isFavorite(item.getLink());

        removeFavorites.setVisible(isFavorite);
        addFavorites.setVisible(!isFavorite);

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.removeFavorites:
                        if (isLogin) {
                            localRssPresenter.removeFavoritesItem(item.getLink());
                        } else {
                            Toast.makeText(getApplicationContext(), R.string.need_login_text, Toast.LENGTH_SHORT).show();
                            getAccount(false);
                        }
                        return true;
                    case R.id.addFavorites:
                        if (isLogin) {
                            localRssPresenter.addFavoritesItem(item);
                        } else {
                            Toast.makeText(getApplicationContext(), R.string.need_login_text, Toast.LENGTH_SHORT).show();
                            getAccount(false);
                        }
                        return true;
                    default:
                        return false;
                }
            }
        });

        popupMenu.show();
    }
}
