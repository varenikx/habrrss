package me.brainstorage.pinbonustz;

import android.app.ProgressDialog;
import android.content.Intent;
import android.util.Log;
import android.view.*;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;
import me.brainstorage.pinbonustz.feeds.LocalRssPresenter;
import me.brainstorage.pinbonustz.feeds.RssItemModel;


public class DetailsPresenter extends ActionBarActivity {
    private WebView detailsView;
    private ProgressDialog pd;
    private LocalRssPresenter localRssPresenter;
    private Boolean addFavoriteOnLogin = false;
    private Boolean removeFavoriteOnLogin = false;
    private Intent accountPresenter;
    private RssItemModel item;
    private Boolean isLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.news_details_layout);

        localRssPresenter = new LocalRssPresenter(
                DetailsPresenter.this
        );

        pd = new ProgressDialog(DetailsPresenter.this);

        Bundle extras = getIntent().getExtras();
        this.isLogin = extras.getBoolean("isLogin");

        item = new RssItemModel();
        item.setLink(extras.getString("link"))
                .setAuthor(extras.getString("autor"))
                .setGuid(extras.getString("guid"))
                .setPubDate(extras.getString("pdate"))
                .setDescription(extras.getString("desc"))
                .setTitle(extras.getString("title"));

        pd.setTitle("Please wait");
        pd.setMessage("Page is loading..");
        pd.show();

        detailsView = (WebView) findViewById(R.id.newsWebDetails);
        detailsView.getSettings().setJavaScriptEnabled(true);
        detailsView.setWebViewClient(new rssWebViewClient());

        detailsView.loadUrl(item.getLink());
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem addFavorites = menu.findItem(R.id.addFavorites);
        MenuItem removeFavorites = menu.findItem(R.id.removeFavorites);

        Boolean isFavorite = localRssPresenter.isFavorite(item.getLink());

        removeFavorites.setVisible(isFavorite);
        addFavorites.setVisible(!isFavorite);

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.news_popup_menu_layout, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int id = menuItem.getItemId();

        if (id == R.id.removeFavorites) {
            if (isLogin) {
                localRssPresenter.removeFavoritesItem(item.getLink());
            } else {
                Toast.makeText(getApplicationContext(), R.string.need_login_text, Toast.LENGTH_SHORT).show();
                this.removeFavoriteOnLogin = true;
                getAccount();
            }
            return true;
        }
        if (id == R.id.addFavorites) {
            if (isLogin) {
                localRssPresenter.addFavoritesItem(item);
            } else {
                Toast.makeText(getApplicationContext(), R.string.need_login_text, Toast.LENGTH_SHORT).show();
                this.addFavoriteOnLogin = true;
                getAccount();
            }
            return true;
        }
        return super.onOptionsItemSelected(menuItem);
    }

    private void getAccount() {
        Intent intent = this.getAccountPresenter();
        intent.putExtra("isLogin", this.isLogin);
        intent.putExtra("showFavoritesList", false);
        startActivityForResult(intent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {
            return;
        }
        this.isLogin = data.getBooleanExtra("isLogin", false);
        if (this.addFavoriteOnLogin) {
            localRssPresenter.addFavoritesItem(item);
            this.addFavoriteOnLogin = false;
        }
        if (this.removeFavoriteOnLogin) {
            localRssPresenter.removeFavoritesItem(item.getLink());
            this.removeFavoriteOnLogin = false;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            destroyPresenter();
        }
        return super.onKeyDown(keyCode, event);
    }

    private void destroyPresenter() {
        Intent intent = new Intent();
        Log.d("DetailsPresneter", String.valueOf(isLogin));
        intent.putExtra("isLogin", isLogin);
        setResult(RESULT_OK, intent);
        finish();
    }

    private Intent getAccountPresenter() {
        if (this.accountPresenter == null) {
            this.accountPresenter = new Intent(getBaseContext(), AccountPresenter.class);
            this.accountPresenter.addFlags(
                    Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP
            );
        }
        return this.accountPresenter;
    }

    private class rssWebViewClient extends WebViewClient {
        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            pd.dismiss();
        }
    }
}
