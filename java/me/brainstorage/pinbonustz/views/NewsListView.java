package me.brainstorage.pinbonustz.views;

import android.content.Context;
import android.text.Html;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import me.brainstorage.pinbonustz.R;
import me.brainstorage.pinbonustz.feeds.RssItemModel;

import java.util.ArrayList;

public class NewsListView extends ListView {

    private ArrayList<RssItemModel> items;

    public NewsListView(Context context) {
        super(context);
    }

    public NewsListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NewsListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setNews(ArrayList<RssItemModel> items) {
        this.items = items;
        this.setAdapter(new NewsItemsAdapter(
                getContext(), this.items
        ));
    }

    private class NewsItemsAdapter extends BaseAdapter {
        private ArrayList<RssItemModel> items;
        private Context context;

        public NewsItemsAdapter(Context context, ArrayList<RssItemModel> items) {
            this.context = context;
            this.items = items;
        }

        @Override
        public int getCount() {
            return this.items.size();
        }

        @Override
        public Object getItem(int position) {
            return this.items.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(
                        Context.LAYOUT_INFLATER_SERVICE
                );
                convertView = inflater.inflate(R.layout.news_layout, null);
            }

            TextView date = (TextView) convertView.findViewById(R.id.date);
            TextView title = (TextView) convertView.findViewById(R.id.title);
            TextView description = (TextView) convertView.findViewById(R.id.shortDescription);

            date.setText(this.items.get(position).getPubDate());
            title.setText(this.items.get(position).getTitle());
            description.setText(
                    Html.fromHtml(this.items.get(position).getDescription())
            );

            return convertView;
        }
    }
}
