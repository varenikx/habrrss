package me.brainstorage.pinbonustz.views;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;
import me.brainstorage.pinbonustz.R;
import me.brainstorage.pinbonustz.feeds.RssChannelModel;

import java.util.ArrayList;
import java.util.List;

public class NewsChannelsView extends ViewPager {

    private ArrayList<RssChannelModel> channels;

    public NewsChannelsView(Context context) {
        super(context);
    }

    public NewsChannelsView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setChannels(ArrayList<RssChannelModel> channels,
                            AdapterView.OnItemClickListener newsTapListener,
                            AdapterView.OnItemLongClickListener newsLongTapListener) {
        this.channels = channels;

        this.setAdapter(new NewsChannelAdapter(channels, newsTapListener, newsLongTapListener));
        this.setCurrentItem(1);
    }


    private class NewsChannelAdapter extends PagerAdapter {
        List<View> pages = new ArrayList<View>();

        public NewsChannelAdapter(ArrayList<RssChannelModel> channels, AdapterView.OnItemClickListener newsTapListener,
                                  AdapterView.OnItemLongClickListener newsLongTapListener) {

            for (int i = 0; i < channels.size(); i++) {
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(
                        Context.LAYOUT_INFLATER_SERVICE
                );

                View channelView = inflater.inflate(R.layout.channel_layout, null);

                TextView date = (TextView) channelView.findViewById(R.id.date);
                TextView description = (TextView) channelView.findViewById(R.id.description);
                NewsListView itemList = (NewsListView) channelView.findViewById(R.id.newsList);

                date.setText(channels.get(i).getPubDate());
                description.setText(Html.fromHtml(channels.get(i).getDescription()));

                itemList.setNews(channels.get(i).getItems());

                itemList.setOnItemLongClickListener(newsLongTapListener);
                itemList.setOnItemClickListener(newsTapListener);

                this.pages.add(channelView);
            }
        }

        @Override
        public Object instantiateItem(View collection, int position) {
            View v = pages.get(position);
            ((ViewPager) collection).addView(v, 0);
            return v;
        }

        @Override
        public void destroyItem(View collection, int position, Object view) {
            ((ViewPager) collection).removeView((View) view);
        }

        @Override
        public int getCount() {
            return channels.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view.equals(object);
        }

        @Override
        public void finishUpdate(View arg0) {
        }

        @Override
        public void restoreState(Parcelable arg0, ClassLoader arg1) {
        }

        @Override
        public Parcelable saveState() {
            return null;
        }

        @Override
        public void startUpdate(View arg0) {
        }
    }
}
